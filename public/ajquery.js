'use strict';

var $ = function(sel, el) {
	return (el || window.document).querySelector(sel);
};
$.create = function(html) {
	var div = document.createElement('div');
	div.innerHTML = html;
	return div;
};
var $$ = function(sel, el) {
	return (el || window.document).querySelectorAll(sel);
};

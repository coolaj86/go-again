// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones

var zones = [];
var zoneMap = {};
var all = document.body
	.querySelector('.wikitable.sortable.jquery-tablesorter')
	.querySelectorAll('tr');
all = [].slice.call(all, 1); // remove header

all.forEach(function(el) {
	if (/Alias|Deprecated|Etc\//.test(el.outerText)) {
		$(el).remove();
		return;
	}

	var fields = [].slice.call(el.querySelectorAll('td'));
	var f = fields.map(function(td) {
		return td.innerText.trim();
	});
	var id = f[5] + ' ' + f[6];
	if (!zoneMap[id]) {
		zones.push([f[2], f[5], f[6]]);
	}
	zoneMap[id] = zoneMap[id] || [];
	zoneMap[id].push(f[2]);
});

// console.log(JSON.stringify(zones));
console.log('Total:', all.length);
console.log('Unique:', Object.keys(zoneMap).length);
console.log(zoneMap);

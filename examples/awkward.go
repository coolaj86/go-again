//#!/usr/bin/env go run
package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Printf("%%s\n")
	fmt.Println(time.Parse(time.RFC3339, "2019-06-21T00:01:09-06:00"))
	fmt.Println(time.Parse(time.RFC3339, "2019-06-21T00:01:09+06:00"))
	fmt.Println(time.Parse(time.RFC3339, "2019-06-21T00:01:09Z"))
	fmt.Println(time.Parse(time.RFC3339, "2019-06-21T00:01:09Z-06:00"))
	fmt.Println(time.Parse(time.RFC3339, "2019-06-21T00:01:09Z+06:00"))
	fmt.Println(time.Date(2020, time.November, 31, 25, 60, 0, 0, time.UTC))
	fmt.Println(time.Date(2016, time.December, 31, 23, 59, 59, 0, time.UTC))

	var loc *time.Location
	loc, _ = time.LoadLocation("America/Denver")
	t := time.Date(2019, time.March, 10, 1, 30, 0, 0, loc)
	fmt.Println(t, "==", t.UTC())
	// 2019-03-10 01:30:00 -0700 MST == 2019-03-10 08:30:00 +0000 UTC
	t = t.Add(time.Duration(1) * time.Hour)
	fmt.Println(t, "==", t.UTC())
	// 2019-03-10 03:30:00 -0600 MDT == 2019-03-10 09:30:00 +0000 UTC

	loc, _ = time.LoadLocation("America/Denver")
	t = time.Date(2019, time.November, 3, 1, 30, 0, 0, loc)
	fmt.Println(t, "==", t.UTC())
	// 2019-11-03 01:30:00 -0600 MDT == 2019-11-03 07:30:00 +0000 UTC
	t = t.Add(time.Duration(1) * time.Hour)
	fmt.Println(t, "==", t.UTC())
	// 2019-11-03 01:30:00 -0700 MST == 2019-11-03 08:30:00 +0000 UTC
	t = t.Add(time.Duration(1) * time.Hour)
	fmt.Println(t, "==", t.UTC())
	// 2019-11-03 02:30:00 -0700 MST == 2019-11-03 09:30:00 +0000 UTC
}

package main

import (
	"fmt"
	"time"
)

func mainx() {
	t1 := time.Date(2019, 06, 16, -1, 31, 0, 0, time.UTC)
	fmt.Println(t1)

	t1 = time.Date(2019, 06, 16, 0, 31, 0, 0, time.UTC)
	fmt.Println(t1)

	t1 = time.Date(2019, 06, 16, 23, 31, 0, 0, time.UTC)
	fmt.Println(t1)

	t1 = time.Date(2019, 06, 16, 24, 31, 0, 0, time.UTC)
	fmt.Println(t1)
}
